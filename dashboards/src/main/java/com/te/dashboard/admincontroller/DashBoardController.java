package com.te.dashboard.admincontroller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.dashboard.adminservices.DashBoardServices;
import com.te.dashboard.responce.Responce;

@RestController
@RequestMapping("/dashboard")
public class DashBoardController {

	@Autowired
	private DashBoardServices services;

	@GetMapping("/piChart/{batchId}")
	public ResponseEntity<Responce> pichart(@PathVariable String batchId) {
		System.err.println("------------------In Controller----------------------");
		Map<String, Float> chart = services.pichart(batchId);
		return new ResponseEntity<Responce>(new Responce(false, "Pi Chart For Gender", chart), HttpStatus.OK);
	}

	@GetMapping("/barGraph/{batchId}")
	public ResponseEntity<Responce> barGraph(@PathVariable String batchId) {
		Map<String, Long> graph = services.barGraph(batchId);
		return new ResponseEntity<Responce>(new Responce(false, "Bar Graph For Year Of Passing", graph), HttpStatus.OK);
	}

	@GetMapping("/barGraphforDegree/{batchId}")
	public ResponseEntity<Responce> barGraphDegree(@PathVariable String batchId) {
		Map<String, Long> graph = services.barGraphDegree(batchId);
		return new ResponseEntity<Responce>(new Responce(false, "Bar Graph For Experience", graph), HttpStatus.OK);
	}

	@GetMapping("/barGraphforExperience/{batchId}")
	public ResponseEntity<Responce> barGraphforExperience(@PathVariable String batchId) {
		Map<String, Long> graph = services.barGraphforExperience(batchId);
		return new ResponseEntity<Responce>(new Responce(false, " BAR_GRAPH_FOR_EMPLOYEE_DEGREE", graph),
				HttpStatus.OK);
	}

	@GetMapping("/piChartforBatchPerformance/{batchId}")
	public ResponseEntity<Responce> piChartforBatchPerformance(@PathVariable String batchId) {
		Map<String, Long> chart = services.piChartforBatchPerformance(batchId);
		return new ResponseEntity<Responce>(new Responce(false, "PI_CHART_FOR_BATCH_PERFORMANCE", chart),
				HttpStatus.OK);
	}
}
