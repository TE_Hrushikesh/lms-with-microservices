package com.te.dashboard.adminservices;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.te.dashboard.customexception.CustomException;
import com.te.dashboard.feign.DashboardFeign;
import com.te.dashboard.feign.Employee;
import com.te.dashboard.feign.EmployeeDetail;
import com.te.dashboard.feign.EmployeeFeign;
import com.te.dashboard.feign.FinalBatch;
import com.te.dashboard.responce.Responce;

import feign.Response;

@Service
public class DashboardServiceImpl implements DashBoardServices {

	@Autowired
	private DashboardFeign repo;

	@Autowired
	private EmployeeFeign repo1;

	@Override
	public Map<String, Float> pichart(String batchId) {
		try {
			FinalBatch batch = repo.getBatchById(batchId);
			if (batch == null) {
				throw new CustomException("SomeThing Went Wrong");

			} else {
				batch.setEmp(repo1.getEmployeeByIds(batch.getEmployeeId()));

				long male = batch.getEmp().stream().filter(t -> t.getGender().equalsIgnoreCase("male")).count();
				long female = batch.getEmp().stream().filter(t -> t.getGender().equalsIgnoreCase("female")).count();
				Float malePercent = Float.valueOf(male) / Float.valueOf(batch.getEmp().size()) * 100f;
				Float femalePercent = Float.valueOf(female) / Float.valueOf(batch.getEmp().size()) * 100f;

				Map<String, Float> chart = new HashMap<>();
				chart.put("Male", malePercent);
				chart.put("Female", femalePercent);

				return chart;
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Map<String, Long> barGraph(String batchId) {
		try {
			FinalBatch batch = repo.getBatchById(batchId);
			batch.setEmp(repo1.getEmployeeByIds(batch.getEmployeeId()));

			List<Employee> emp = batch.getEmp();
			Map<String, Long> chart = new HashMap<>();
			chart.put("2019", emp.stream().filter(t -> t.getEducationDetails().getYearOfPassing() == 2019).count());
			chart.put("2020", emp.stream().filter(t -> t.getEducationDetails().getYearOfPassing() == 2020).count());
			chart.put("2021", emp.stream().filter(t -> t.getEducationDetails().getYearOfPassing() == 2021).count());
			chart.put("2022", emp.stream().filter(t -> t.getEducationDetails().getYearOfPassing() == 2022).count());
			chart.put("2023", emp.stream().filter(t -> t.getEducationDetails().getYearOfPassing() == 2023).count());
			return chart;

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Map<String, Long> barGraphDegree(String batchId) {
		try {
			FinalBatch batch = repo.getBatchById(batchId);
			batch.setEmp(repo1.getEmployeeByIds(batch.getEmployeeId()));
			List<Employee> emp = batch.getEmp();
			Map<String, Long> graph = new HashMap<>();
			graph.put("Graduation", emp.stream()
					.filter(t -> t.getEducationDetails().getEducationType().equalsIgnoreCase("Graduation")).count());
			graph.put("Post Graduation",
					emp.stream()
							.filter(t -> t.getEducationDetails().getEducationType().equalsIgnoreCase("Post Graduation"))
							.count());
			return graph;

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Map<String, Long> barGraphforExperience(String batchId) {
		try {
			FinalBatch batch = repo.getBatchById(batchId);
			batch.setEmp(repo1.getEmployeeByIds(batch.getEmployeeId()));
			List<Employee> emp = batch.getEmp();
			Map<String, Long> graph = new HashMap<>();
			List<Stream<Integer>> collect = emp.stream().map(e -> e.getExperiences().stream().map(t -> {
				return t.getYearsOfExperience();
			})).collect(Collectors.toList());
			List<Integer> collect2 = collect.stream().flatMap(e -> e).collect(Collectors.toList());
			graph.put("Fresher", collect2.stream().filter(t -> t == 0).count());
			graph.put("1", collect2.stream().filter(t -> t == 1).count());
			graph.put("2", collect2.stream().filter(t -> t == 2).count());
			graph.put("5", collect2.stream().filter(t -> t == 5).count());
			graph.put("10", collect2.stream().filter(t -> t == 10).count());
			return graph;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Map<String, Long> piChartforBatchPerformance(String batchId) {
		try {
			FinalBatch batch = repo.getBatchById(batchId);
			batch.setEmp(repo1.getEmployeeByIds(batch.getEmployeeId()));
			List<EmployeeDetail> list = repo.getEmployeeByBatchId(batchId);
			List<List<String>> collect = list.stream().map(e -> e.getMockRatings().stream().map(t -> {
				return t.getOverAllFeedback();
			})).collect(Collectors.toList()).stream().flatMap(e -> e).collect(Collectors.toList());
			List<String> collect2 = collect.stream().flatMap(t -> t.stream()).collect(Collectors.toList());
			Map<String, Long> graph = new HashMap<>();
			graph.put("Excellent", collect2.stream().filter(t -> t.equalsIgnoreCase("Excellent")).count());
			graph.put("Good", collect2.stream().filter(t -> t.equalsIgnoreCase("Good")).count());
			graph.put("Above Average", collect2.stream().filter(t -> t.equalsIgnoreCase("Above Average")).count());
			graph.put("Average", collect2.stream().filter(t -> t.equalsIgnoreCase("Average")).count());
			graph.put("Below Average", collect2.stream().filter(t -> t.equalsIgnoreCase("Below Average")).count());
			return graph;

		} catch (Exception e) {
			throw e;
		}
	}
}
