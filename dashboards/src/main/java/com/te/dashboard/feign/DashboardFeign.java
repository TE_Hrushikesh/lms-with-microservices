package com.te.dashboard.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "MENTOR-SERVICES", url = "http://localhost:9003/mentorFeign")
public interface DashboardFeign {

	@GetMapping("/getbatch/{batchId}")
	public FinalBatch getBatchById(@PathVariable String batchId);

	@GetMapping("/getemployeeByBatchId/{batchId}")
	public java.util.List<EmployeeDetail> getEmployeeByBatchId(@PathVariable String batchId);

}
