package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class MockRatings {

	private int id;

	private List<String> mockType;

	private List<String> technology;

	private String mockTakenBy;

	private int practicalKnowledge;

	private int theoreticalKnowledge;

	List<String> overAllFeedback;

	private String detailedFeedback;
}
