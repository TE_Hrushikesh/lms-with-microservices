package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class FinalBatch {

	private int finalBatchNo;

	private int batchNo;

	private String batchId;

	private String batchName;

	private List<String> technologies;

	private String startDate;

	private String endDate;

	private List<String> employeeId;

	private List<Employee> emp;

	private String batchStatus;

	private int strength;
}
