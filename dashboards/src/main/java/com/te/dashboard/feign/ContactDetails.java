package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class ContactDetails {

	private int contactDetailsId;

	private List<String> contactType;

	private long contactNumber;
}