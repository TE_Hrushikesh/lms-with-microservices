package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class SecondaryInfo {

	private int secondaryInfoId;

	private String panNumber;

	private String aadharNumber;

	private String fatherName;

	private String motherName;

	private String spouseName;

	private String passpoprtNumber;

	private List<String> marritalStatus;
}
