package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class EmployeeDetail {

	private int no;

	private String employeeId;

	private String batchId;

	private String employeeName;

	private int mockTaken;

	private List<MockRatings> mockRatings;

	private int attendance;

	private String employeeStatus;
}
