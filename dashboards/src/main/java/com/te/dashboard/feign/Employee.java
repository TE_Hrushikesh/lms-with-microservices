package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class Employee {

	private int employeeDetailsId;

	private String employeeId;

	private String employeeName;

	private String dateOfJoining;

	private String dateOfBirth;

	private String email;

	private String bloodGroup;

	private List<String> designation;

	private String gender;

	private List<String> nationality;

	private List<String> employeeStatus;

	private SecondaryInfo secondaryInfo;

	private EducationDetails educationDetails;

	private List<AddressDetails> addressDetails;

	private BankDetails bankDetails;

	private List<TechnicalSkills> technicalskills;

	private List<Experience> experiences;

	private List<ContactDetails> contactDetails;
}
