package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class Experience {

	private int experienceId;

	private String companyName;

	private int yearsOfExperience;

	private String dateOfJoinning;

	private String dateOfRelieving;

	private List<String> designation;

	private String location;
}
