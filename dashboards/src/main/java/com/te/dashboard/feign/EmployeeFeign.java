package com.te.dashboard.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "EMPLOYEE-SERVICES", url = "http://localhost:9002/employee")
public interface EmployeeFeign {

	@GetMapping("/getByEmployeeId/{employeeId}")
	public Employee getEmployee(@PathVariable String employeeId);

	@GetMapping("/getByEmployeeIdList")
	public List<Employee> getEmployeeByIds(@RequestParam List<String> employeeId);
}
