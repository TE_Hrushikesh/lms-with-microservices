package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class BankDetails {

	private int bankDetailsId;

	private long accountNumber;

	private String bankName;

	private List<String> accountType;

	private String ifscCode;

	private List<String> branch;

	private List<String> state;
}