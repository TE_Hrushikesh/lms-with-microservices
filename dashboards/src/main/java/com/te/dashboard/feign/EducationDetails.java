package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class EducationDetails {

	private int educationDetailsId;

	private String educationType;

	private int yearOfPassing;

	private double percentage;

	private List<String> universityName;

	private String instituteName;

	private String specialization;

	private List<String> state;
}
