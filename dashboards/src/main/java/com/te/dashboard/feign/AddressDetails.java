package com.te.dashboard.feign;

import java.util.List;

import lombok.Data;

@Data
public class AddressDetails {

	private int addressDetailsId;

	private List<String> addressType;

	private int doorNumber;

	private String street;

	private String locality;

	private List<String> city;

	private List<String> state;

	private int pincode;

	private String landmark;
}