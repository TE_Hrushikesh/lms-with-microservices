package com.te.employeeservice.employeerepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.employeeservice.employeeentity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	Optional<Employee> findByEmail(String email);

	Optional<Employee> findByEmployeeId(String employeeId);

	Page<Employee> findAllByEmployeeIdContainingIgnoreCaseOrEmployeeNameContainingIgnoreCaseOrDateOfJoiningContainingIgnoreCaseOrDateOfBirthContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBloodGroupContainingIgnoreCase(
			String str, String str2, String str3, String str4, String str5, String str6, Pageable p);

	List<Employee> findByEmployeeIdIn(List<String> employeeId);
}
