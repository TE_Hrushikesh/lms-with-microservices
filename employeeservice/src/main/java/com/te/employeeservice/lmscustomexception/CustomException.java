package com.te.employeeservice.lmscustomexception;

@SuppressWarnings("serial")
public class CustomException extends RuntimeException{

	public CustomException(String s) {
		super(s);
	}
}
