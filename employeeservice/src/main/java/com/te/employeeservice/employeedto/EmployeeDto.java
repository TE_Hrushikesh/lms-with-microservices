package com.te.employeeservice.employeedto;

import java.util.List;

import com.te.employeeservice.employeeentity.AddressDetails;
import com.te.employeeservice.employeeentity.BankDetails;
import com.te.employeeservice.employeeentity.ContactDetails;
import com.te.employeeservice.employeeentity.EducationDetails;
import com.te.employeeservice.employeeentity.Experience;
import com.te.employeeservice.employeeentity.SecondaryInfo;
import com.te.employeeservice.employeeentity.TechnicalSkills;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto {

	private String employeeId = "TYSS";

	private String employeeName;

	private String dateOfJoining;

	private String dateOfBirth;

	private String email;

	private String bloodGroup;

	private List<String> designation;

	private String gender;

	private List<String> nationality;

	private List<String> employeeStatus;

	private SecondaryInfo secondaryInfo;

	private EducationDetails educationDetails;

	private List<AddressDetails> addressDetails;

	private BankDetails bankDetails;

	private List<TechnicalSkills> technicalskills;

	private List<Experience> experiences;

	private List<ContactDetails> contactDetails;
}
