package com.te.employeeservice.lmsresponce;

import java.util.List;

import javax.persistence.Convert;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationResponce {

	private boolean error;
	
	@Convert(converter = ListToString.class)
	private List<String> message;
	
	private Object data;
}
