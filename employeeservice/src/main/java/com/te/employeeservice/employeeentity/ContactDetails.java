package com.te.employeeservice.employeeentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
@Entity
public class ContactDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int contactDetailsId;

	@Convert(converter = ListToString.class)
	private List<String> contactType;

	private long contactNumber;
}