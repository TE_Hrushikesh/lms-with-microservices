package com.te.employeeservice.employeeentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int employeeDetailsId;

	private String employeeId;

	private String employeeName;

	private String dateOfJoining;

	private String dateOfBirth;

	private String email;

	private String bloodGroup;

	@Convert(converter = ListToString.class)
	private List<String> designation;

	private String gender;

	@Convert(converter = ListToString.class)
	private List<String> nationality;

	@Convert(converter = ListToString.class)
	private List<String> employeeStatus;

	@OneToOne
	@Cascade(CascadeType.ALL)
	private SecondaryInfo secondaryInfo;

	@OneToOne
	@Cascade(CascadeType.ALL)
	private EducationDetails educationDetails;

	@OneToMany
	@Cascade(CascadeType.ALL)
	@JoinColumn(name = "employeeDetailsId")
	private List<AddressDetails> addressDetails;

	@OneToOne
	@Cascade(CascadeType.ALL)
	private BankDetails bankDetails;

	@OneToMany
	@Cascade(CascadeType.ALL)
	@JoinColumn(name = "employeeDetailsId")
	private List<TechnicalSkills> technicalskills;

	@OneToMany
	@Cascade(CascadeType.ALL)
	@JoinColumn(name = "employeeDetailsId")
	private List<Experience> experiences;

	@OneToMany
	@Cascade(CascadeType.ALL)
	@JoinColumn(name = "employeeDetailsId")
	private List<ContactDetails> contactDetails;
}
