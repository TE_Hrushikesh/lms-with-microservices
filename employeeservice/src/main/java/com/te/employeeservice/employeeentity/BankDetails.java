package com.te.employeeservice.employeeentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
@Entity
public class BankDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bankDetailsId;

	private long accountNumber;

	private String bankName;

	@Convert(converter = ListToString.class)
	private List<String> accountType;

	private String ifscCode;

	@Convert(converter = ListToString.class)
	private List<String> branch;

	@Convert(converter = ListToString.class)
	private List<String> state;
}