package com.te.employeeservice.employeeentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Entity
@Data
public class AddressDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int addressDetailsId;

	@Convert(converter = ListToString.class)
	private List<String> addressType;

	private int doorNumber;

	private String street;

	private String locality;

	@Convert(converter = ListToString.class)
	private List<String> city;

	@Convert(converter = ListToString.class)
	private List<String> state;

	private int pincode;

	private String landmark;
}