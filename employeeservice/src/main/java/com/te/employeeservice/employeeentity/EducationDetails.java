package com.te.employeeservice.employeeentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
@Entity
public class EducationDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int educationDetailsId;

	private String educationType;

	private int yearOfPassing;

	private double percentage;

	@Convert(converter = ListToString.class)
	private List<String> universityName;

	private String instituteName;

	private String specialization;

	@Convert(converter = ListToString.class)
	private List<String> state;
}
