package com.te.employeeservice.employeeentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Entity
@Data
public class SecondaryInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int secondaryInfoId;

	private String panNumber;

	private String aadharNumber;

	private String fatherName;

	private String motherName;

	private String spouseName;

	private String passpoprtNumber;

	@Convert(converter = ListToString.class)
	private List<String> marritalStatus;
}
