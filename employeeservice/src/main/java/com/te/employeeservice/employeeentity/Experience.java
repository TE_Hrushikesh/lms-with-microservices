package com.te.employeeservice.employeeentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.employeeservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Entity
@Data
public class Experience {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int experienceId;

	private String companyName;

	private int yearsOfExperience;

	private String dateOfJoinning;

	private String dateOfRelieving;

	@Convert(converter = ListToString.class)
	private List<String> designation;

	private String location;
}
