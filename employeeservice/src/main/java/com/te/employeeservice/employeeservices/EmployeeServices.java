package com.te.employeeservice.employeeservices;

import java.util.List;

import com.te.employeeservice.employeedto.EmployeeDto;
import com.te.employeeservice.employeeentity.Employee;
import com.te.employeeservice.lmsresponce.PageResponce;

public interface EmployeeServices {

	Employee addEmployee(EmployeeDto employeeDto);

	Employee getEmployeeDetails(int id);

	Employee updateEmployee(EmployeeDto employeeDto);

	PageResponce getAllEmployeeDetails(int pageNumber, int pageSize, String str);

	void deleteEmployee(String id);

	Employee getEmployee(String employeeId);

	List<Employee> getEmployeeById(List<String> employeeId);
}
