package com.te.adminservice.admincontroller;

import static com.te.adminservice.lmsconstants.Constants.ALL_BATCH_DETAILS;
import static com.te.adminservice.lmsconstants.Constants.BATCH_ADDED_SUCCESFULLY;
import static com.te.adminservice.lmsconstants.Constants.BATCH_DELETED_SUCCESSFULLY;
import static com.te.adminservice.lmsconstants.Constants.BATCH_DETAILS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.adminservice.admindto.BatchDto;
import com.te.adminservice.admindto.NewBatchPojo;
import com.te.adminservice.adminentity.Batch;
import com.te.adminservice.adminservices.BatchService;
import com.te.adminservice.lmsresponce.PageResponce;
import com.te.adminservice.lmsresponce.Responce;

@RestController
@RequestMapping("/batch")
public class BatchController {

	@Autowired
	private BatchService service;

	@PostMapping("/addBatch")
	public ResponseEntity<Responce> addBatch(@RequestBody BatchDto batchDto) {
		BatchDto batch = service.addBatch(batchDto);
		return new ResponseEntity<Responce>(new Responce(false, BATCH_ADDED_SUCCESFULLY, batch), HttpStatus.OK);
	}

	@GetMapping("/getBatchDetails/{id}")
	public ResponseEntity<Responce> addBatch(@PathVariable int id) {
		Batch batch = service.getBatchDetails(id);
		return new ResponseEntity<Responce>(new Responce(false, BATCH_DETAILS, batch), HttpStatus.OK);
	}

	@DeleteMapping("/deleteBatchDetails/{id}")
	public ResponseEntity<Responce> deleteBatchDetails(@PathVariable int id) {
		service.deleteBatchDetails(id);
		return new ResponseEntity<Responce>(new Responce(false, BATCH_DELETED_SUCCESSFULLY, null), HttpStatus.OK);
	}

	@GetMapping("/getAllBatchDetails/{pageNumber}/{pageSize}/{name}")
	public ResponseEntity<Responce> getAllBatchDetails(@PathVariable int pageNumber, @PathVariable int pageSize,
			@PathVariable String name) {
		PageResponce batch = service.getAllBatchDetails(pageNumber, pageSize, name);
		return new ResponseEntity<Responce>(new Responce(false, ALL_BATCH_DETAILS, batch), HttpStatus.OK);
	}

	@PutMapping("/updateBatchDetail")
	public ResponseEntity<Responce> updateBatchDetail(@RequestBody NewBatchPojo batchPojo) {
		Batch batch = service.updateBatchDetail(batchPojo);
		return new ResponseEntity<Responce>(new Responce(false, BATCH_DETAILS, batch), HttpStatus.OK);
	}

	@GetMapping("/getbatch/{batchId}")
	public BatchDto getbatch(@PathVariable int batchId) {
		BatchDto dto = service.getBatch(batchId);
		return dto;
	}
}
