package com.te.adminservice.admindto;

import java.util.List;

import com.te.adminservice.adminentity.Batch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MentorDto {

	
	private String mentorName;
	
	private String employeeId="TYSS";
	
	private String email;
	
	private List<String> skills;
	
	private List<Batch> batch;
}
