package com.te.adminservice.admindto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatchDto {

	private String batchName;

	private int mentorNo;

	private String mentorName;

	private String batchId = "Batch";

	private List<String> technologies;

	private String startDate;

	private String endDate;

	private List<String> status;
}
