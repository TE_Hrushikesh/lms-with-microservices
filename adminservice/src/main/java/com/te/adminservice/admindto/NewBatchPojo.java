package com.te.adminservice.admindto;

import java.util.List;

import com.te.adminservice.adminentity.Mentor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewBatchPojo {

	private int batchNo;
	
	private String batchName;

	private Mentor mentor;

	private String batchId;

	private List<String> technologies;

	private String startDate;

	private String endDate;

	private List<String> status;
}
