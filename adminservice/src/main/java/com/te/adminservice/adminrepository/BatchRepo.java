package com.te.adminservice.adminrepository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.adminservice.adminentity.Batch;

@Repository
public interface BatchRepo extends JpaRepository<Batch, Integer> {

	Page<Batch> findAllByBatchNameContainingIgnoreCaseOrBatchIdContainingIgnoreCaseOrStartDateContainingIgnoreCaseOrEndDateContainingIgnoreCase(
			String str, String str2, String str3, String str4, PageRequest p);

	Optional<Batch> findByBatchId(String batchId);
}
