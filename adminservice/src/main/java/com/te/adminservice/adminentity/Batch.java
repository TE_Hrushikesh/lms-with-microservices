package com.te.adminservice.adminentity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.te.adminservice.lmslisttostringconverter.ListToString;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString.Exclude;

@JsonInclude(value = Include.NON_NULL)
@Entity
@Data
@NoArgsConstructor
public class Batch {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int batchNo;

	private String batchName;

	@Exclude
	@JsonBackReference
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Mentor mentor;

	private String batchId;

	@Convert(converter = ListToString.class)
	private List<String> technologies;

	private String startDate;

	private String endDate;

	@Convert(converter = ListToString.class)
	private List<String> status;
}
