package com.te.adminservice.adminentity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.te.adminservice.lmslisttostringconverter.ListToString;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString.Exclude;

@JsonInclude(value = Include.NON_NULL)
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Mentor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int mentorNo;

	private String mentorName;

	private String employeeId;

	private String email;

	@Convert(converter = ListToString.class)
	private List<String> skills;

	@Exclude
	@JsonManagedReference
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "mentorNo")
	private List<Batch> batch;
}