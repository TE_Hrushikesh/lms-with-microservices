package com.te.adminservice.adminservices;

import com.te.adminservice.admindto.BatchDto;
import com.te.adminservice.admindto.NewBatchPojo;
import com.te.adminservice.adminentity.Batch;
import com.te.adminservice.lmsresponce.PageResponce;

public interface BatchService {

	BatchDto addBatch(BatchDto batchDto);

	Batch getBatchDetails(int id);

	void deleteBatchDetails(int id);

	PageResponce getAllBatchDetails(int pageNumber, int pageSize, String name);

	Batch updateBatchDetail(NewBatchPojo batchPojo);

	BatchDto getBatch(int batchId);
}
