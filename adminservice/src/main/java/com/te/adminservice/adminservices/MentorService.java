package com.te.adminservice.adminservices;


import com.te.adminservice.admindto.MentorDto;
import com.te.adminservice.adminentity.Mentor;
import com.te.adminservice.lmsresponce.PageResponce;

public interface MentorService {

	Mentor addMentor(MentorDto dto);

	Mentor getMentor(int id);

	String deleteMentorDetails(int id);

	PageResponce getAllMentorDetails(int pageNumber, int pageSize, String str);

	Mentor updateMentorDetails(MentorDto dto);
}
