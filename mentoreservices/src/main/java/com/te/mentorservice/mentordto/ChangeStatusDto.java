package com.te.mentorservice.mentordto;

import lombok.Data;

@Data
public class ChangeStatusDto {
	
	private String employeeId;
	
	private String employeeStatus;
	
	private String reasonForChangeStatus;
}
