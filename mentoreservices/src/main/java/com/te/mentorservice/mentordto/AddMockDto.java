package com.te.mentorservice.mentordto;

import java.util.List;

import lombok.Data;

@Data
public class AddMockDto {

	private List<String> batchId;

	private int mockNo;

	private List<String> technology;

	private List<String> panel;

	private String dateAndTime;
}
