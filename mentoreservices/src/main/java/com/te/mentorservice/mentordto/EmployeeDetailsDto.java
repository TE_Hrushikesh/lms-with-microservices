package com.te.mentorservice.mentordto;

import java.util.List;

import com.te.mentorservice.mentorentity.MockRatings;

import lombok.Data;

@Data
public class EmployeeDetailsDto {

	private String employeeId;

	private String batchId;

	private int mockTaken;

	private List<MockRatings> mockRatings;

	private int attendance;

	private String employeeStatus;
}
