package com.te.mentorservice.mentorcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.mentorservice.mentorentity.EmployeeDetail;
import com.te.mentorservice.mentorentity.FinalBatch;
import com.te.mentorservice.mentorservices.Mentorservices;

@RestController
@RequestMapping("/mentorFeign")
public class FeignController {

	@Autowired
	private Mentorservices services;

	@GetMapping("/getbatch/{batchId}")
	public FinalBatch getBatchById(@PathVariable String batchId) {
		FinalBatch batch = services.getBatchById(batchId);
		return batch;
	}

	@GetMapping("/getemployeeByBatchId/{batchId}")
	public java.util.List<EmployeeDetail> getEmployeeByBatchId(@PathVariable String batchId) {
		java.util.List<EmployeeDetail> emp = services.getemployeeByBatchId(batchId);
		return emp;
	}
}
