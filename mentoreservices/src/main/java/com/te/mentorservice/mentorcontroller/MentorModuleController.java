package com.te.mentorservice.mentorcontroller;

import static com.te.mentorservice.constants.Constants.ADDING_MOCK;
import static com.te.mentorservice.constants.Constants.ASSIGNING_BATCH;
import static com.te.mentorservice.constants.Constants.ASSIGNING_EMPLOYEE_RATINGS;
import static com.te.mentorservice.constants.Constants.BATCH_ASSIGN_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.BATCH_LIST;
import static com.te.mentorservice.constants.Constants.CHANGE_STATUS_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.CHANGING_STATUS_OF_EMPLOYEE;
import static com.te.mentorservice.constants.Constants.EMPLOYEE_RATINGS_ASSIGN_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.GETTING_ALL_BATCH_LIST;
import static com.te.mentorservice.constants.Constants.MOCK_ADDED_SUCCESFULLY;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.mentorservice.lmsresponce.Responce;
import com.te.mentorservice.mentordto.AddMockDto;
import com.te.mentorservice.mentordto.ChangeStatusDto;
import com.te.mentorservice.mentordto.EmployeeDetailsDto;
import com.te.mentorservice.mentordto.FinalBatchDto;
import com.te.mentorservice.mentorentity.AddMock;
import com.te.mentorservice.mentorentity.EmployeeDetail;
import com.te.mentorservice.mentorentity.FinalBatch;
import com.te.mentorservice.mentorservices.Mentorservices;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/mentor")
public class MentorModuleController {

	@Autowired
	private Mentorservices services;

	@PostMapping("/assignFinalBatch")
	public ResponseEntity<Responce> assignBatch(@RequestBody FinalBatchDto batchDto) {
		log.info(ASSIGNING_BATCH);
		FinalBatch batch = services.assignBatch(batchDto);
		return new ResponseEntity<Responce>(new Responce(false, BATCH_ASSIGN_SUCCESFULLY, batch), HttpStatus.OK);
	}

	@GetMapping("/batchList")
	public ResponseEntity<Responce> getBatchList() {
		log.info(GETTING_ALL_BATCH_LIST);
		List<FinalBatch> batch = services.getBatchList();
		return new ResponseEntity<Responce>(new Responce(false, BATCH_LIST, batch), HttpStatus.OK);
	}

	@PostMapping("/employeeRatings")
	public ResponseEntity<Responce> employeeRatings(@RequestBody EmployeeDetailsDto empdto) {
		log.info(ASSIGNING_EMPLOYEE_RATINGS);
		EmployeeDetail emp = services.employeeRatings(empdto);
		return new ResponseEntity<Responce>(new Responce(false, EMPLOYEE_RATINGS_ASSIGN_SUCCESFULLY, emp),
				HttpStatus.OK);
	}

	@PostMapping("/addMock")
	public ResponseEntity<Responce> addMock(@RequestBody AddMockDto mockdto) {
		log.info(ADDING_MOCK);
		AddMock emp = services.employeeRatings(mockdto);
		return new ResponseEntity<Responce>(new Responce(false, MOCK_ADDED_SUCCESFULLY, emp), HttpStatus.OK);
	}

	@PostMapping("/changeStatus")
	public ResponseEntity<Responce> changeStatus(@RequestBody ChangeStatusDto dto) {
		log.info(CHANGING_STATUS_OF_EMPLOYEE);
		services.changeStatus(dto);
		return new ResponseEntity<Responce>(new Responce(false, CHANGE_STATUS_SUCCESFULLY, null), HttpStatus.OK);
	}

//	@GetMapping("/getbatch")
//	public FinalBatch getBatchById(@PathVariable String batchId) {
//		FinalBatch batch = services.getBatchById(batchId);
//		return batch;
//	}
}
