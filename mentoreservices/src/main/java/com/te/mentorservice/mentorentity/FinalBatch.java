package com.te.mentorservice.mentorentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.te.mentorservice.feign.Employee;
import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
@Entity
@JsonInclude(value = Include.NON_NULL)
public class FinalBatch {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int finalBatchNo;

	private int batchNo;

	private String batchId;

	private String batchName;

	@Convert(converter = ListToString.class)
	private List<String> technologies;

	private String startDate;

	private String endDate;

	@Convert(converter = ListToString.class)
	private List<String> employeeId;

	@Transient
	private List<Employee> emp;

	private String batchStatus;

	private int strength;
}
