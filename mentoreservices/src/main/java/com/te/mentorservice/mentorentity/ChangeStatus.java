package com.te.mentorservice.mentorentity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class ChangeStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String employeeId;

	private String employeeName;

	private String employeeStatus;

	private String reasonForChangeStatus;
}
