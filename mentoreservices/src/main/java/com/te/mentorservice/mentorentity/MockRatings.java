package com.te.mentorservice.mentorentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
@Entity
public class MockRatings {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Convert(converter = ListToString.class)
	private List<String> mockType;

	@Convert(converter = ListToString.class)
	private List<String> technology;

	private String mockTakenBy;

	private int practicalKnowledge;

	private int theoreticalKnowledge;

	@Convert(converter = ListToString.class)
	List<String> overAllFeedback;

	private String detailedFeedback;
}
