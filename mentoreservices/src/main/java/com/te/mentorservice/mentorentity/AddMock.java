package com.te.mentorservice.mentorentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
@Entity
public class AddMock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Convert(converter = ListToString.class)
	private List<String> batchId;

	private int mockNo;

	@Convert(converter = ListToString.class)
	private List<String> technology;

	@Convert(converter = ListToString.class)
	private List<String> panel;

	private String dateAndTime;
}
