package com.te.mentorservice.feign;

import java.util.List;

import javax.persistence.Convert;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
public class Experience {

	private int experienceId;

	private String companyName;

	private int yearsOfExperience;

	private String dateOfJoinning;

	private String dateOfRelieving;

	@Convert(converter = ListToString.class)
	private List<String> designation;

	private String location;
}
