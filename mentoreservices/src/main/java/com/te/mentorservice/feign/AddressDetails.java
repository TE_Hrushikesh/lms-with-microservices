package com.te.mentorservice.feign;

import java.util.List;

import javax.persistence.Convert;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
public class AddressDetails {

	private int addressDetailsId;

	@Convert(converter = ListToString.class)
	private List<String> addressType;

	private int doorNumber;

	private String street;

	private String locality;

	@Convert(converter = ListToString.class)
	private List<String> city;

	@Convert(converter = ListToString.class)
	private List<String> state;

	private int pincode;

	private String landmark;
}