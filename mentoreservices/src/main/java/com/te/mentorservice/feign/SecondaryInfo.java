package com.te.mentorservice.feign;

import java.util.List;

import javax.persistence.Convert;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
public class SecondaryInfo {

	private int secondaryInfoId;

	private String panNumber;

	private String aadharNumber;

	private String fatherName;

	private String motherName;

	private String spouseName;

	private String passpoprtNumber;

	@Convert(converter = ListToString.class)
	private List<String> marritalStatus;
}
