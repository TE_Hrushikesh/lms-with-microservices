package com.te.mentorservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ADMIN-SERVICES"/* , url = "http://localhost:9001/batch" */)
public interface BatchFeign {

	@GetMapping("/batch/getbatch/{batchId}")
	public BatchDto getbatch(@PathVariable int batchId);
}
