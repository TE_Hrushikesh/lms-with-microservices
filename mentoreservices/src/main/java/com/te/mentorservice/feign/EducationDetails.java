package com.te.mentorservice.feign;

import java.util.List;

import javax.persistence.Convert;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
public class EducationDetails {

	private int educationDetailsId;

	private String educationType;

	private int yearOfPassing;

	private double percentage;

	@Convert(converter = ListToString.class)
	private List<String> universityName;

	private String instituteName;

	private String specialization;

	@Convert(converter = ListToString.class)
	private List<String> state;
}
