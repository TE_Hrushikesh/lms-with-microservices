package com.te.mentorservice.feign;

import java.util.List;

import javax.persistence.Convert;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
public class BankDetails {

	private int bankDetailsId;

	private long accountNumber;

	private String bankName;

	@Convert(converter = ListToString.class)
	private List<String> accountType;

	private String ifscCode;

	@Convert(converter = ListToString.class)
	private List<String> branch;

	@Convert(converter = ListToString.class)
	private List<String> state;
}