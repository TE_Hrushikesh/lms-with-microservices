package com.te.mentorservice.feign;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MentorDto {

	private String mentorName;

	private String employeeId = "TYSS";

	private String email;

	private List<String> skills;

	private List<BatchDto> batch;
}
