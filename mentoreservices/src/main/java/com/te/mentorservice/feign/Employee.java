package com.te.mentorservice.feign;

import java.util.List;

import javax.persistence.Convert;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
public class Employee {

	private int employeeDetailsId;

	private String employeeId;

	private String employeeName;

	private String dateOfJoining;

	private String dateOfBirth;

	private String email;

	private String bloodGroup;

	@Convert(converter = ListToString.class)
	private List<String> designation;

	private String gender;

	@Convert(converter = ListToString.class)
	private List<String> nationality;

	@Convert(converter = ListToString.class)
	private List<String> employeeStatus;

	private SecondaryInfo secondaryInfo;

	private EducationDetails educationDetails;

	private List<AddressDetails> addressDetails;

	private BankDetails bankDetails;

	private List<TechnicalSkills> technicalskills;

	private List<Experience> experiences;

	private List<ContactDetails> contactDetails;
}
