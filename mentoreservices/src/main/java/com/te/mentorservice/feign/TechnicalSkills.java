package com.te.mentorservice.feign;

import lombok.Data;

@Data
public class TechnicalSkills {

	private int technicalSkillsId;

	private String skillType;

	private int skillRating;

	private int yearOfExperience;
}
