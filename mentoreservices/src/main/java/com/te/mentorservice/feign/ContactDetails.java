package com.te.mentorservice.feign;

import java.util.List;

import javax.persistence.Convert;

import com.te.mentorservice.lmslisttostringconverter.ListToString;

import lombok.Data;

@Data
public class ContactDetails {

	private int contactDetailsId;

	@Convert(converter = ListToString.class)
	private List<String> contactType;

	private long contactNumber;
}