package com.te.mentorservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "EMPLOYEE-SERVICES",url = "http://localhost:9002/employee")
public interface EmployeeFeign {

	@GetMapping("/getByEmployeeId/{employeeId}")
	public Employee getEmployee(@PathVariable String employeeId);

}
