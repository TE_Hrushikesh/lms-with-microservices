package com.te.mentorservice.mentorservices;

import java.util.List;

import com.te.mentorservice.mentordto.AddMockDto;
import com.te.mentorservice.mentordto.ChangeStatusDto;
import com.te.mentorservice.mentordto.EmployeeDetailsDto;
import com.te.mentorservice.mentordto.FinalBatchDto;
import com.te.mentorservice.mentorentity.AddMock;
import com.te.mentorservice.mentorentity.EmployeeDetail;
import com.te.mentorservice.mentorentity.FinalBatch;

public interface Mentorservices {

	FinalBatch assignBatch(FinalBatchDto batchDto);

	List<FinalBatch> getBatchList();

	EmployeeDetail employeeRatings(EmployeeDetailsDto empdto);

	AddMock employeeRatings(AddMockDto mockdto);

	void changeStatus(ChangeStatusDto dto);

	FinalBatch getBatchById(String batchId);

	List<EmployeeDetail> getemployeeByBatchId(String batchId);
}
