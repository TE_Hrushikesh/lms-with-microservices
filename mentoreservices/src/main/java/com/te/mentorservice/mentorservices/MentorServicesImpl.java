package com.te.mentorservice.mentorservices;

import static com.te.mentorservice.constants.Constants.ADDING_MOCK;
import static com.te.mentorservice.constants.Constants.ADDING_MOCK_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.ASSIGNING_BATCH;
import static com.te.mentorservice.constants.Constants.ASSIGNING_EMPLOYEE_RATING;
import static com.te.mentorservice.constants.Constants.ASSIGNING_EMPLOYEE_RATINGS_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.BATCH_ASSIGNED_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.CHANGING_STATUS_OF_EMPLOYEE;
import static com.te.mentorservice.constants.Constants.CHANGING_STATUS_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.EMPLOYEE_WHICH_YOU_GIVING_RATING_IS_NOT_PRESENT_ON_THIS_EMPLOYEE_ID;
import static com.te.mentorservice.constants.Constants.GETTING_FINAL_BATCH_DETAILS_SUCCESFULLY;
import static com.te.mentorservice.constants.Constants.GETTING_FINAL_BATCH_LIST;
import static com.te.mentorservice.constants.Constants.NO_ONE_BATCH_IS_AVAILABLE_AT_THE_MOMENT;
import static com.te.mentorservice.constants.Constants.WHICH_BATCH_YOU_ASSIGN_IS_NOT_PRESENT;
import static com.te.mentorservice.constants.Constants.WHICH_EMPLOYEE_STATUS_YOU_WANT_TO_CHANGE_IS_NOT_AVAILABLE;
import static com.te.mentorservice.constants.Constants.WHICH_EMPLOYEE_YOU_ADD_IS_NOT_PRESENT_AND_THAT_EMPLOYEE_ID_IS;
import static com.te.mentorservice.constants.Constants.WHICH_EMPLOYEE_YOU_ADD_IS_NOT_PRESENT_AND_THAT_EMPLOYEE_ID_IS2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.mentorservice.feign.BatchDto;
import com.te.mentorservice.feign.BatchFeign;
import com.te.mentorservice.feign.Employee;
import com.te.mentorservice.feign.EmployeeFeign;
import com.te.mentorservice.lmscustomexception.CustomException;
import com.te.mentorservice.mentordto.AddMockDto;
import com.te.mentorservice.mentordto.ChangeStatusDto;
import com.te.mentorservice.mentordto.EmployeeDetailsDto;
import com.te.mentorservice.mentordto.FinalBatchDto;
import com.te.mentorservice.mentorentity.AddMock;
import com.te.mentorservice.mentorentity.ChangeStatus;
import com.te.mentorservice.mentorentity.EmployeeDetail;
import com.te.mentorservice.mentorentity.FinalBatch;
import com.te.mentorservice.mentorrepository.AddMockRepo;
import com.te.mentorservice.mentorrepository.EmployeeDetailsRepo;
import com.te.mentorservice.mentorrepository.FinalBatchRepo;
import com.te.mentorservice.mentorrepository.StatusChange;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MentorServicesImpl implements Mentorservices {

	private static final String GETTING_FINAL_BATCH = null;

	private static final String NO_ONE_EMPLOYEE_IS_AVAILABLE_AT_THE_MOMENT = null;

	private static final String GETTING_EMPLOTYEE_DETAILS_SUCCESFULLY = null;

	@Autowired
	private StatusChange statusRepo;

	@Autowired
	private BatchFeign batchRepo;

	@Autowired
	private FinalBatchRepo repo;

	@Autowired
	private EmployeeFeign feign;

	@Autowired
	private EmployeeDetailsRepo employeeDetailsRepo;

	@Autowired
	private AddMockRepo mockrepo;

	@Override
	public FinalBatch assignBatch(FinalBatchDto dto) {
		try {
			log.info(ASSIGNING_BATCH);
			BatchDto findById = batchRepo.getbatch(dto.getBatchNo());
			if (findById.equals(null)) {
				log.error(WHICH_BATCH_YOU_ASSIGN_IS_NOT_PRESENT);
				throw new CustomException(WHICH_BATCH_YOU_ASSIGN_IS_NOT_PRESENT);
			} else {
				FinalBatch batch = new FinalBatch();
				List<Employee> emp = new ArrayList<>();
				for (int i = 0; i < dto.getEmployeeId().size(); i++) {
					Employee findByEmployeeId = feign.getEmployee(dto.getEmployeeId().get(i));
					if (findByEmployeeId == null) {
						log.error(WHICH_EMPLOYEE_YOU_ADD_IS_NOT_PRESENT_AND_THAT_EMPLOYEE_ID_IS
								+ dto.getEmployeeId().get(i));
						throw new CustomException(WHICH_EMPLOYEE_YOU_ADD_IS_NOT_PRESENT_AND_THAT_EMPLOYEE_ID_IS2
								+ dto.getEmployeeId().get(i));
					}
					emp.add(findByEmployeeId);
				}
				BeanUtils.copyProperties(findById, batch);
				BeanUtils.copyProperties(dto, batch);
				batch.setStrength(emp.size());
				batch.setEmp(emp);
				log.info(BATCH_ASSIGNED_SUCCESFULLY);
				FinalBatch save = repo.save(batch);
				return save;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<FinalBatch> getBatchList() {
		try {
			log.info(GETTING_FINAL_BATCH_LIST);
			List<FinalBatch> findAll = repo.findAll();
			if (findAll.isEmpty()) {
				log.error(NO_ONE_BATCH_IS_AVAILABLE_AT_THE_MOMENT);
				throw new CustomException(NO_ONE_BATCH_IS_AVAILABLE_AT_THE_MOMENT);
			} else {
				log.info(GETTING_FINAL_BATCH_DETAILS_SUCCESFULLY);
				return findAll;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public EmployeeDetail employeeRatings(EmployeeDetailsDto empdto) {
		try {
			log.info(ASSIGNING_EMPLOYEE_RATING);
			Employee employee = feign.getEmployee(empdto.getEmployeeId());
			if (employee == null) {
				log.error(EMPLOYEE_WHICH_YOU_GIVING_RATING_IS_NOT_PRESENT_ON_THIS_EMPLOYEE_ID);
				throw new CustomException(EMPLOYEE_WHICH_YOU_GIVING_RATING_IS_NOT_PRESENT_ON_THIS_EMPLOYEE_ID);
			} else {
				EmployeeDetail detail = new EmployeeDetail();
				BeanUtils.copyProperties(employee, detail);
				BeanUtils.copyProperties(empdto, detail);
				EmployeeDetail save = employeeDetailsRepo.save(detail);
				log.info(ASSIGNING_EMPLOYEE_RATINGS_SUCCESFULLY);
				return save;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public AddMock employeeRatings(AddMockDto mockdto) {
		try {
			log.info(ADDING_MOCK);
			AddMock mock = new AddMock();
			BeanUtils.copyProperties(mockdto, mock);
			AddMock save = mockrepo.save(mock);
			log.info(ADDING_MOCK_SUCCESFULLY);
			return save;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public void changeStatus(ChangeStatusDto dto) {
		try {
			log.info(CHANGING_STATUS_OF_EMPLOYEE);
			Optional<EmployeeDetail> detail = employeeDetailsRepo.findByEmployeeId(dto.getEmployeeId());
			if (!detail.isPresent()) {
				log.error(WHICH_EMPLOYEE_STATUS_YOU_WANT_TO_CHANGE_IS_NOT_AVAILABLE);
				throw new CustomException(WHICH_EMPLOYEE_STATUS_YOU_WANT_TO_CHANGE_IS_NOT_AVAILABLE);
			} else {
				ChangeStatus changeStatus = new ChangeStatus();
				BeanUtils.copyProperties(dto, changeStatus);
				changeStatus.setEmployeeName(detail.get().getEmployeeName());
				detail.get().setEmployeeStatus(dto.getEmployeeStatus());
				employeeDetailsRepo.save(detail.get());
				log.info(CHANGING_STATUS_SUCCESFULLY);
				statusRepo.save(changeStatus);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public FinalBatch getBatchById(String batchId) {
		try {
			log.info(GETTING_FINAL_BATCH);
			Optional<FinalBatch> batch = repo.findByBatchId(batchId);
			if (!batch.isPresent()) {
				log.error(NO_ONE_BATCH_IS_AVAILABLE_AT_THE_MOMENT);
				throw new CustomException(NO_ONE_BATCH_IS_AVAILABLE_AT_THE_MOMENT);
			} else {
				log.info(GETTING_FINAL_BATCH_DETAILS_SUCCESFULLY);
				return batch.get();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<EmployeeDetail> getemployeeByBatchId(String batchId) {
		try {
			List<EmployeeDetail> employees = employeeDetailsRepo.findByBatchId(batchId);
			if (employees.isEmpty()) {
				log.error(NO_ONE_EMPLOYEE_IS_AVAILABLE_AT_THE_MOMENT);
				throw new CustomException(NO_ONE_EMPLOYEE_IS_AVAILABLE_AT_THE_MOMENT);
			} else {
				log.info(GETTING_EMPLOTYEE_DETAILS_SUCCESFULLY);
				return employees;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}
}
