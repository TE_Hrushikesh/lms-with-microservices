package com.te.mentorservice.mentorrepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.mentorservice.mentorentity.EmployeeDetail;

@Repository
public interface EmployeeDetailsRepo extends JpaRepository<EmployeeDetail, Integer> {

	Optional<EmployeeDetail> findByEmployeeId(String employeeId);

	List<EmployeeDetail> findByBatchId(String batchId);

}
