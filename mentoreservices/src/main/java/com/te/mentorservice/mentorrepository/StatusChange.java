package com.te.mentorservice.mentorrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.mentorservice.mentorentity.ChangeStatus;

@Repository
public interface StatusChange extends JpaRepository<ChangeStatus, Integer>{

}
