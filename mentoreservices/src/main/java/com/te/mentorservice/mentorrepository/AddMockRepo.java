package com.te.mentorservice.mentorrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.mentorservice.mentorentity.AddMock;

@Repository
public interface AddMockRepo extends JpaRepository<AddMock, Integer> {

}
